import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./utilities/app-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { StaffSalaryModule } from "./pages/staffSalary/staff-salary.module";
import { LayoutComponent } from "./pages/layout/layout.component";
import { ToastrModule } from "ngx-toastr";

@NgModule({
  declarations: [AppComponent, LayoutComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StaffSalaryModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
    }),
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
