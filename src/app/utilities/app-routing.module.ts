import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { FormStaffSalaryComponent } from "../pages/staffSalary/form-staff-salary/form-staff-salary.component";
import { ListStaffSalaryComponent } from "../pages/staffSalary/list-staff-salary/list-staff-salary.component";
import { LayoutComponent } from "../pages/layout/layout.component";

const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "",
        redirectTo: "staff",
        pathMatch: "full",
      },
      { path: "staff", component: ListStaffSalaryComponent },
      { path: "staff/new", component: FormStaffSalaryComponent },
      { path: "**", redirectTo: "staff", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
