export let percentages = {
  percentageOtherIncome: 0.08,
  percentageCompensationBonus: 0.75,
};
export let meses = [
  { Text: "Enero", Value: 1 },
  { Text: "Febrero", Value: 2 },
  { Text: "Marzo", Value: 3 },
  { Text: "Abril", Value: 4 },
  { Text: "Mayo", Value: 5 },
  { Text: "Junio", Value: 6 },
  { Text: "Julio", Value: 7 },
  { Text: "Agosto", Value: 8 },
  { Text: "Septiembre", Value: 9 },
  { Text: "Octubre", Value: 10 },
  { Text: "Noviembre", Value: 11 },
  { Text: "Diciembre", Value: 12 },
];
export class UtilsMethods {
  getMonthNames(value): string {
    return meses.find((x) => x.Value === Number(value)).Text;
  }
  Years(): any[] {
    var d = new Date();
    var y = d.getFullYear();
    var years = [];
    for (var i = y; i >= 2000; i--) {
      years.push(i);
    }
    return years;
  }
}
