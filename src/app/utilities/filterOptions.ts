export enum FilterOptions {
  SameOffice = 1, // mismas oficina y grado
  AllOffices, // Todas las oficinas y el mismo grado
  SamePosition, // misma posicion y Guardado
  AllPositions, // todas las posiciones y el mismo grado
  EmployeeCode, // por código del empleado
}
