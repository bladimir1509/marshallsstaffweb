import {
  AbstractControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
} from "@angular/forms";

export function validateSalariesDate(salaries): ValidatorFn {
  return (form: FormGroup): ValidationErrors | null => {
    if (salaries.length === 0) {
      return null;
    }
    let index = salaries.findIndex(
      (x) => x.year === form.value.year && x.month === form.value.month
    );
    if (index >= 0 && index !== form.value.idSalary) {
      form.controls.year.setErrors({ existSalary: true });
      form.controls.month.setErrors({ existSalary: true });
    } else {
      if (form.value.year !== "") form.controls.year.setErrors(null);
      if (form.value.month !== "") form.controls.month.setErrors(null);
    }

    return null;
  };
}

export function onlyDecimal(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    var RE = /^[0-9.]+$/;
    if (!RE.test(control.value)) {
      return { onlyDecimal: true };
    } else {
      return null;
    }
  };
}
export function onlyNumber(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    debugger;
    var RE = /^([0-9])*$/;
    if (!RE.test(control.value)) {
      return { onlyNumber: true };
    } else {
      return null;
    }
  };
}
