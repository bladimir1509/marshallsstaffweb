export const BASE_URL = "https://localhost:44369/api/";

export let API_ROUTES = {
  staffSalary: BASE_URL + "staffSalary",
  catalogs: BASE_URL + "catalogs/",
  positions: BASE_URL + "catalogs/positions",
  divisions: BASE_URL + "catalogs/divisions",
  offices: BASE_URL + "catalogs/offices",
};
