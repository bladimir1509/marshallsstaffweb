export interface Catalogs {
  id: number;
  name: string;
}
export enum CatalogsNum {
  Position = 1,
  Division,
  Office,
}
export class ConvertCatalogs {
  public static toCatalogs(json: any): Catalogs {
    return JSON.parse(json);
  }

  public static catalogsToJson(value: Catalogs): string {
    return JSON.stringify(value);
  }
}
