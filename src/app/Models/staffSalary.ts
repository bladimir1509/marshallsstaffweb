export interface StaffSalary {
  year: number;
  month: number;
  office: number;
  employeeCode: string;
  employeeName: string;
  employeeSurname: string;
  division: number;
  position: number;
  grade: number;
  beginDate: Date;
  birthday: Date;
  identificationNumber: string;
  baseSalary: number;
  productionBonus: number;
  compensationBonus: number;
  commission: number;
  contributions: number;
  id: number;
}

// Converts JSON strings to/from your types
export class ConvertStaff {
  public static toStaffSalary(json: string): StaffSalary {
    return JSON.parse(json);
  }

  public static staffSalaryToJson(value: StaffSalary): string {
    return JSON.stringify(value);
  }
}
