import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ListStaffSalaryComponent } from "./list-staff-salary/list-staff-salary.component";
import { FormStaffSalaryComponent } from "./form-staff-salary/form-staff-salary.component";
import { ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { GroupByPipe } from "src/app/utilities/group-by.pipe";
import { ControlMessages } from "src/app/utilities/control-messages.component";

@NgModule({
  declarations: [
    ListStaffSalaryComponent,
    FormStaffSalaryComponent,
    GroupByPipe,
    ControlMessages,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    RouterModule,
    FormsModule,
  ],
  exports: [ListStaffSalaryComponent, FormStaffSalaryComponent, GroupByPipe],
})
export class StaffSalaryModule {}
