import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { Catalogs, CatalogsNum } from "src/app/Models/catalogs";
import { ResponseApi } from "src/app/Models/ResponseApi";
import { StaffSalary } from "src/app/Models/staffSalary";
import { CatalogsService } from "src/app/services/catalogs.service";
import { StaffSalaryService } from "src/app/services/staff-salary.service";
import { UtilsMethods, meses } from "src/app/utilities/constants";
import {
  onlyDecimal,
  onlyNumber,
  validateSalariesDate,
} from "src/app/utilities/customsValidations";
import { Location } from "@angular/common";
@Component({
  selector: "app-form-staff-salary",
  templateUrl: "./form-staff-salary.component.html",
  styleUrls: ["./form-staff-salary.component.css"],
})
export class FormStaffSalaryComponent implements OnInit {
  public formSalary: FormGroup;
  public formStaff: FormGroup;
  formStaffInvalid = false;
  formSalaryInvalid = false;
  utils: UtilsMethods = new UtilsMethods();

  positions: Catalogs[];
  divisions: Catalogs[];
  offices: Catalogs[];
  salaries: StaffSalary[] = [];
  editSalary = false;
  showSalariesForm = false;
  months;
  years;

  constructor(
    private readonly _formB: FormBuilder,
    private _serviceSalaryStaff: StaffSalaryService,
    private _serviceCatalogs: CatalogsService,
    private _toastr: ToastrService,
    private location: Location
  ) {}

  ngOnInit() {
    this.months = meses;
    this.years = this.utils.Years();
    this.GetCatalogs();
    this.InitForms();
  }

  GetCatalogs() {
    this._serviceCatalogs
      .getPositions()
      .subscribe((catalogs: ResponseApi) => (this.positions = catalogs.data));

    this._serviceCatalogs
      .getDivisions()
      .subscribe((catalogs: ResponseApi) => (this.divisions = catalogs.data));

    this._serviceCatalogs
      .getOffices()
      .subscribe((catalogs: ResponseApi) => (this.offices = catalogs.data));
  }
  public InitForms() {
    this.formSalary = this._formB.group(
      {
        idSalary: null,
        year: [null, Validators.required],
        month: [null, Validators.required],
        office: [null, Validators.required],
        division: [null, Validators.required],
        position: [null, Validators.required],
        grade: [null, [Validators.required, onlyNumber(), Validators.min(1)]],
        beginDate: [null, Validators.required],
        baseSalary: [null, [Validators.required, onlyDecimal()]],
        productionBonus: [0, [Validators.required, onlyDecimal()]],
        compensationBonus: [0, [Validators.required, onlyDecimal()]],
        commission: [0, [Validators.required, onlyDecimal()]],
        contributions: [0, [Validators.required, onlyDecimal()]],
      },
      {
        validator: [validateSalariesDate(this.salaries)],
      }
    );
    this.formStaff = this._formB.group({
      employeeCode: [
        null,
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
          onlyNumber(),
        ],
      ],
      employeeName: [null, Validators.required],
      employeeSurname: [null, Validators.required],
      birthday: [null, Validators.required],
      identificationNumber: [
        null,
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
          onlyNumber(),
        ],
      ],
    });
  }

  SaveStaffSalaries() {
    let staffSalary: StaffSalary;
    if (this.formStaff.invalid || this.salaries.length === 0) {
      this.formStaffInvalid = true;
      this._toastr.error(
        "El formulario es inválido o no ha ingresado salarios para el empleado",
        "ERROR"
      );
      return;
    }
    this.formStaffInvalid = false;
    for (let i = 0; i < this.salaries.length; i++) {
      staffSalary = Object.assign(this.formStaff.value, this.salaries[i]);

      this._serviceSalaryStaff.postStaffSalaries(staffSalary).subscribe(
        (resp: StaffSalary) => {
          this._toastr.success(
            `La información del empleado ${resp.employeeName} ${resp.employeeSurname} ha sido registrada correctamente`,
            "Guardado"
          );
          this.location.back();
          console.log(resp);
        },
        (error) => {
          this._toastr.error(error.error.errors[0].detail, "Error");
        }
      );
    }
  }

  getNameCatalog(num: CatalogsNum, id: number): string {
    switch (num) {
      case CatalogsNum.Position:
        return this.positions.find((p) => p.id === Number(id)).name;

      case CatalogsNum.Division:
        return this.divisions.find((p) => p.id === Number(id)).name;

      case CatalogsNum.Office:
        return this.offices.find((p) => p.id === Number(id)).name;

      default:
        return "";
    }
  }
  AddSalary() {
    if (this.formSalary.invalid) {
      this.formSalaryInvalid = true;
      this._toastr.error("El formulario es inválido", "ERROR");

      return;
    }
    this.formSalaryInvalid = false;
    if (this.editSalary) {
      this.salaries[this.formSalary.value.idSalary] = this.formSalary.value;
      this._toastr.success("Se actualizó el salario Correctamente", "Editado");
    } else {
      this.editSalary = false;
      this._toastr.success("Se registró el salario Correctamente", "Guardado");

      this.salaries.push(this.formSalary.value);
    }

    this.resetFormSalary();
  }

  RemoveSalary(staff) {
    let index = this.salaries.findIndex(
      (x) => x.year == staff.year && x.month == staff.month
    );

    this.salaries.splice(index, 1);
  }
  chargeDataFormSalary(salary, i) {
    this.formSalary.patchValue(salary);
    this.formSalary.patchValue({ idSalary: i });
    this.editSalary = true;
  }

  resetFormSalary() {
    this.formSalaryInvalid = false;
    this.formSalary.reset();
    this.formSalary.patchValue({
      productionBonus: 0,
      compensationBonus: 0,
      commission: 0,
      contributions: 0,
    });
    this.editSalary = false;
  }
}
