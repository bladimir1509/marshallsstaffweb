import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { FormStaffSalaryComponent } from "./form-staff-salary.component";

xdescribe("FormStaffSalaryComponent", () => {
  let component: FormStaffSalaryComponent;
  let fixture: ComponentFixture<FormStaffSalaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormStaffSalaryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormStaffSalaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
