import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  Catalogs,
  CatalogsNum,
  ConvertCatalogs,
} from "src/app/Models/catalogs";
import { ResponseApi } from "src/app/Models/ResponseApi";
import { StaffSalary } from "src/app/Models/staffSalary";
import { CatalogsService } from "src/app/services/catalogs.service";
import { StaffSalaryService } from "src/app/services/staff-salary.service";
import { percentages, UtilsMethods } from "src/app/utilities/constants";
import { FilterOptions } from "src/app/utilities/filterOptions";
import { GroupByPipe } from "src/app/utilities/group-by.pipe";

@Component({
  selector: "app-list-staff-salary",
  templateUrl: "./list-staff-salary.component.html",
  styleUrls: ["./list-staff-salary.component.css"],
})
export class ListStaffSalaryComponent implements OnInit {
  positions: Catalogs[];
  divisions: Catalogs[];
  offices: Catalogs[];
  showRelated = false;
  groupBy: GroupByPipe = new GroupByPipe();
  EmployeeRelated: StaffSalary;
  inputEmployeeCode = "";
  staffOriginal: StaffSalary[];
  employeeLastSalaries: StaffSalary[] = [];
  utils: UtilsMethods = new UtilsMethods();
  staffSalaries: StaffSalary[];
  Bonus: number = 0;
  total;
  p = 0;
  constructor(
    public router: Router,
    private _serviceSalaryStaff: StaffSalaryService,
    private _serviceCatalogs: CatalogsService
  ) {}

  ngOnInit() {
    this.GetCatalogs();
    this.GetStaffSalaries();
  }

  GetStaffSalaries() {
    this._serviceSalaryStaff
      .getStaffSalaries()
      .subscribe((staffSalary: ResponseApi) => {
        this.staffSalaries = staffSalary.data;
        this.total = this.groupBy.transform(
          staffSalary.data,
          "employeeCode"
        ).length;
        this.staffOriginal = this.staffSalaries;
      });
  }

  GetCatalogs() {
    this._serviceCatalogs
      .getPositions()
      .subscribe((catalogs: ResponseApi) => (this.positions = catalogs.data));

    this._serviceCatalogs
      .getDivisions()
      .subscribe((catalogs: ResponseApi) => (this.divisions = catalogs.data));

    this._serviceCatalogs
      .getOffices()
      .subscribe((catalogs: ResponseApi) => (this.offices = catalogs.data));
  }

  getNameCatalog(num: CatalogsNum, id: number): string {
    switch (num) {
      case CatalogsNum.Position:
        return this.positions.find((p) => p.id === id).name;

      case CatalogsNum.Division:
        return this.divisions.find((p) => p.id === id).name;

      case CatalogsNum.Office:
        return this.offices.find((p) => p.id === id).name;

      default:
        return "";
    }
  }

  CalculateTotalSalary(staffSalary: StaffSalary): number {
    const otherIncome =
      (staffSalary.baseSalary + staffSalary.commission) *
        percentages.percentageOtherIncome +
      staffSalary.commission;

    const totalSalary =
      staffSalary.baseSalary +
      staffSalary.productionBonus +
      staffSalary.compensationBonus * percentages.percentageCompensationBonus +
      otherIncome;

    return totalSalary;
  }
  setRelatedEmployees(staff) {
    if (staff !== null) {
      this.showRelated = true;
      this.EmployeeRelated = staff.value[0];
      this.searchRelatedEmployees(1);
    } else {
      this.showRelated = false;
      this.EmployeeRelated = staff;
      this.staffSalaries = this.staffOriginal;
    }
  }
  searchRelatedEmployees(option: FilterOptions) {
    switch (option) {
      case FilterOptions.SameOffice:
        this.staffSalaries = this.staffOriginal.filter(
          (s) =>
            s.office === this.EmployeeRelated.office &&
            s.grade === this.EmployeeRelated.grade &&
            s.employeeCode !== this.EmployeeRelated.employeeCode
        );
        break;
      case FilterOptions.AllOffices:
        this.staffSalaries = this.staffOriginal.filter(
          (s) =>
            s.grade === this.EmployeeRelated.grade &&
            s.employeeCode !== this.EmployeeRelated.employeeCode
        );
        break;
      case FilterOptions.SamePosition:
        this.staffSalaries = this.staffOriginal.filter(
          (s) =>
            s.position === this.EmployeeRelated.position &&
            s.grade === this.EmployeeRelated.grade &&
            s.employeeCode !== this.EmployeeRelated.employeeCode
        );
        break;
      case FilterOptions.AllPositions:
        this.staffSalaries = this.staffOriginal.filter(
          (s) =>
            s.grade === this.EmployeeRelated.grade &&
            s.employeeCode !== this.EmployeeRelated.employeeCode
        );
        break;
      case FilterOptions.EmployeeCode:
        this.employeeLastSalaries = [];
        this.staffSalaries = this.staffOriginal.filter(
          (s) => s.employeeCode === this.inputEmployeeCode
        );
        let lastSalary = this.staffSalaries
          .sort((a, b) => b.year - a.year)
          .filter((x) => x.year === this.staffSalaries[0].year)
          .sort((a, b) => b.month - a.month)[0];
        if (lastSalary) {
          this.employeeLastSalaries.push(lastSalary);
          this.Bonus = lastSalary.baseSalary;
          let { month, year } = lastSalary;
          for (let i = 0; i < 2; i++) {
            month--;
            if (month === 0) {
              month = 12;
              year = year - 1;
            }
            let salary = this.staffSalaries.find(
              (x) => x.month === month && x.year === year
            );
            if (salary) {
              this.employeeLastSalaries.push(salary);
              this.Bonus = this.Bonus + salary.baseSalary;
            }
          }
          this.Bonus = this.Bonus / 3;
        }

        break;
    }
  }

  clearSearch() {
    this.staffSalaries = this.staffOriginal;
    this.inputEmployeeCode = "";
    this.employeeLastSalaries = [];
    this.Bonus = 0;
  }
}
