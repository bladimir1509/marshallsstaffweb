import { HttpClientTestingModule } from "@angular/common/http/testing";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { NgxPaginationModule } from "ngx-pagination";
import { Catalogs, CatalogsNum } from "src/app/Models/catalogs";
import { StaffSalary } from "src/app/Models/staffSalary";
import { FilterOptions } from "src/app/utilities/filterOptions";
import { GroupByPipe } from "src/app/utilities/group-by.pipe";

import { ListStaffSalaryComponent } from "./list-staff-salary.component";

describe("ListStaffSalaryComponent", () => {
  let component: ListStaffSalaryComponent;
  let fixture: ComponentFixture<ListStaffSalaryComponent>;

  const catalogs = [
    { id: 1, name: "test 1" },
    { id: 2, name: "test 2" },
  ] as Catalogs[];
  const salaries = [
    {
      id: 1,
      year: 2021,
      month: 1,
      office: 6,
      employeeCode: "4943716869",
      employeeName: "Margi",
      employeeSurname: "Walczak",
      division: 3,
      position: 3,
      grade: 19,
      beginDate: new Date("2021-10-12"),
      birthday: new Date("1996-09-15"),
      identificationNumber: "6513461492",
      baseSalary: 606.82,
      productionBonus: 60.26,
      compensationBonus: 539.67,
      commission: 935.25,
      contributions: 354,
    },
    {
      id: 2,
      year: 2021,
      month: 1,
      office: 6,
      employeeCode: "6044725375",
      employeeName: "Lucie",
      employeeSurname: "Radcliffe",
      division: 2,
      position: 3,
      grade: 19,
      beginDate: new Date("2020-08-01"),
      birthday: new Date("1992-06-23"),
      identificationNumber: "5534071473",
      baseSalary: 58.21,
      productionBonus: 179.34,
      compensationBonus: 733.28,
      commission: 767.24,
      contributions: 594,
    },
  ] as StaffSalary[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        NgxPaginationModule,
        HttpClientTestingModule,
      ],
      declarations: [ListStaffSalaryComponent, GroupByPipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListStaffSalaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  it("getNameCatalog deberia retornar información de la división", () => {
    component.divisions = catalogs;
    component.offices = catalogs;
    component.positions = catalogs;
    let id = Math.floor(Math.random() * 2) + 1;

    let nameDivision = component.getNameCatalog(CatalogsNum.Division, id);
    let nameOffice = component.getNameCatalog(CatalogsNum.Office, id);
    let namePosition = component.getNameCatalog(CatalogsNum.Position, id);

    expect(catalogs.find((x) => x.id === id).name).toEqual(nameDivision);
    expect(catalogs.find((x) => x.id === id).name).toEqual(nameOffice);
    expect(catalogs.find((x) => x.id === id).name).toEqual(namePosition);
  });

  it("CalculateTotalSalary deberia retornar el salario total del empleado", () => {
    let totalSalary = component.CalculateTotalSalary(salaries[0]);

    expect(totalSalary).toEqual(2130.4481);
  });
  it("searchRelatedEmployees deberia buscar los empleados relacionado según criterio misma oficina y el mismo grado", () => {
    component.staffOriginal = salaries;
    let staff = salaries[Math.floor(Math.random() * 1)];
    component.EmployeeRelated = staff;

    component.searchRelatedEmployees(FilterOptions.SameOffice);
    let relates = component.staffSalaries.filter(
      (x) => x.office === staff.office && x.grade === staff.grade
    );
    expect(component.staffSalaries.length).toEqual(relates.length);
  });

  it("searchRelatedEmployees deberia buscar los empleados relacionado según criterio todas las oficinas y el mismo grado y todas las posiciones y el mismo grado", () => {
    component.staffOriginal = salaries;
    let staff = salaries[Math.floor(Math.random() * 1)];
    component.EmployeeRelated = staff;

    component.searchRelatedEmployees(FilterOptions.AllOffices);
    let relates = component.staffSalaries.filter(
      (x) => x.grade === staff.grade
    );
    expect(component.staffSalaries.length).toEqual(relates.length);
  });

  it("searchRelatedEmployees deberia buscar los empleados relacionado según criterio misma posición y el mismo grado", () => {
    component.staffOriginal = salaries;
    let staff = salaries[Math.floor(Math.random() * 1)];
    component.EmployeeRelated = staff;

    component.searchRelatedEmployees(FilterOptions.SamePosition);
    let relates = component.staffSalaries.filter(
      (x) => x.position === staff.position && x.grade === staff.grade
    );
    expect(component.staffSalaries.length).toEqual(relates.length);
  });

  it("searchRelatedEmployees deberia buscar el empleado por el código y mostrar sus ultimos salarios", () => {
    component.staffOriginal = salaries;
    let staff = salaries[Math.floor(Math.random() * 1)];
    component.EmployeeRelated = staff;
    component.inputEmployeeCode = staff.employeeCode;

    component.searchRelatedEmployees(FilterOptions.EmployeeCode);
    let search = component.staffOriginal.filter(
      (x) => x.employeeCode == staff.employeeCode
    );
    expect(component.staffSalaries.length).toEqual(search.length);
    expect(component.employeeLastSalaries.length).toBeGreaterThanOrEqual(
      search.length
    );
    expect(component.Bonus).not.toEqual(0);
  });

  it("clearSearch reinicia la busqueda realizada", () => {
    component.staffOriginal = salaries;
    let staff = salaries[Math.floor(Math.random() * 1)];
    component.EmployeeRelated = staff;

    component.inputEmployeeCode = staff.employeeCode;

    component.searchRelatedEmployees(FilterOptions.EmployeeCode);
    let search = component.staffOriginal.filter(
      (x) => x.employeeCode == staff.employeeCode
    );

    component.clearSearch();

    expect(component.staffSalaries).toEqual(component.staffOriginal);
    expect(component.inputEmployeeCode).toBe("");
    expect(component.employeeLastSalaries.length).toEqual(0);
    expect(component.Bonus).toEqual(0);
  });
});
