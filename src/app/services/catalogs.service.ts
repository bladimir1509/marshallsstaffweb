import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Catalogs, ConvertCatalogs } from "../Models/catalogs";
import { API_ROUTES, BASE_URL } from "../utilities/apiRoutes";

@Injectable({
  providedIn: "root",
})
export class CatalogsService {
  catalogs: ConvertCatalogs;
  constructor(private http: HttpClient) {}

  getPositions() {
    return this.http.get(API_ROUTES.positions);
  }

  getDivisions() {
    return this.http.get(API_ROUTES.divisions);
  }

  getOffices() {
    return this.http.get(API_ROUTES.offices);
  }
}
