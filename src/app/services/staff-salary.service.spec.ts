import { TestBed } from "@angular/core/testing";

import { StaffSalaryService } from "./staff-salary.service";
import {
  HttpClientTestingModule,
  HttpTestingController,
} from "@angular/common/http/testing";
import { StaffSalary } from "../Models/staffSalary";
import { HttpErrorResponse, HttpRequest } from "@angular/common/http";
import { API_ROUTES } from "../utilities/apiRoutes";
import { throwError } from "rxjs/internal/observable/throwError";

describe("StaffSalaryService", () => {
  const salaries = [
    {
      id: 1,
      year: 2021,
      month: 1,
      office: 5,
      employeeCode: "4943716869",
      employeeName: "Margi",
      employeeSurname: "Walczak",
      division: 3,
      position: 3,
      grade: 19,
      beginDate: new Date("2021-10-12"),
      birthday: new Date("1996-09-15"),
      identificationNumber: "6513461492",
      baseSalary: 606.82,
      productionBonus: 60.26,
      compensationBonus: 539.67,
      commission: 935.25,
      contributions: 354,
    },
    {
      id: 2,
      year: 2021,
      month: 1,
      office: 6,
      employeeCode: "6044725375",
      employeeName: "Lucie",
      employeeSurname: "Radcliffe",
      division: 2,
      position: 3,
      grade: 20,
      beginDate: new Date("2020-08-01"),
      birthday: new Date("1992-06-23"),
      identificationNumber: "5534071473",
      baseSalary: 58.21,
      productionBonus: 179.34,
      compensationBonus: 733.28,
      commission: 767.24,
      contributions: 594,
    },
  ] as StaffSalary[];
  let staffSalaryService: StaffSalaryService,
    httpTestingController: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [StaffSalaryService],
    });
    staffSalaryService = TestBed.get(StaffSalaryService);
    httpTestingController = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    httpTestingController.verify(); //Verifies that no requests are outstanding.
  });

  it("should be created", () => {
    expect(staffSalaryService).toBeTruthy();
    // httpTestingController.configureTestingModule()
  });

  it("getStaffSalaries deberia retornar una lista de salarios de los empleados", () => {
    staffSalaryService.getStaffSalaries().subscribe((resp) => {
      expect(resp).toEqual(salaries);
    });
    const request = httpTestingController.expectOne((req: HttpRequest<any>) =>
      req.url.includes(API_ROUTES.staffSalary)
    );
    request.flush(salaries);
  });

  it("postStaffSalaries deberia insertar una empleado con su salario ", () => {
    staffSalaryService.postStaffSalaries(salaries[0]).subscribe((resp) => {
      expect(resp).toEqual(salaries[0]);
    });
    const request = httpTestingController.expectOne((req: HttpRequest<any>) =>
      req.url.includes(API_ROUTES.staffSalary)
    );
    expect(request.request.method).toEqual("POST");
    request.flush(salaries[0]);
  });
  it("postStaffSalaries deberia retornar un error al insertar un empleado", () => {
    const status = 500;
    const statusText = "Internal Server Error";
    const errorEvent = new ErrorEvent("API error");

    let actualError: HttpErrorResponse | undefined;

    staffSalaryService.postStaffSalaries(salaries[0]).subscribe(
      (data) => expect(data).toEqual(salaries[0], "should return the employee"),
      (error) => (actualError = error)
    );

    const req = httpTestingController.expectOne(API_ROUTES.staffSalary);

    /* … */
    req.error(errorEvent, { status, statusText });
    expect(actualError.error).toBe(errorEvent);
    expect(actualError.status).toBe(status);
    expect(actualError.statusText).toBe(statusText);

    // expect(req.request.).toEqual(msg);
  });
});
