import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { API_ROUTES } from "../utilities/apiRoutes";

@Injectable({
  providedIn: "root",
})
export class StaffSalaryService {
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
    }),
  };
  constructor(private http: HttpClient) {}

  getStaffSalaries() {
    return this.http.get(API_ROUTES.staffSalary);
  }

  postStaffSalaries(staffSalary) {
    return this.http.post(
      API_ROUTES.staffSalary,
      staffSalary,
      this.httpOptions
    );
  }
}
