import { HttpRequest } from "@angular/common/http";
import {
  HttpClientTestingModule,
  HttpTestingController,
} from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { Catalogs } from "../Models/catalogs";
import { API_ROUTES } from "../utilities/apiRoutes";

import { CatalogsService } from "./catalogs.service";

describe("CatalogsService", () => {
  let catalogsService: CatalogsService,
    httpTestingController: HttpTestingController;

  const catalogs = [
    { id: 1, name: "test 1" },
    { id: 2, name: "test 2" },
  ] as Catalogs[];
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CatalogsService],
    });
    catalogsService = TestBed.get(CatalogsService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it("should be created", () => {
    expect(catalogsService).toBeTruthy();
  });

  it("getPositions retorna un catalogo de posiciones", () => {
    catalogsService.getPositions().subscribe((resp) => {
      expect(resp).toEqual(catalogs);
    });
    const request = httpTestingController.expectOne((req: HttpRequest<any>) =>
      req.url.includes(API_ROUTES.positions)
    );
    request.flush(catalogs);
    httpTestingController.verify();
  });

  it("getDivisions retorna un catalogo de divisiones", () => {
    catalogsService.getDivisions().subscribe((resp) => {
      expect(resp).toEqual(catalogs);
    });
    const request = httpTestingController.expectOne((req: HttpRequest<any>) =>
      req.url.includes(API_ROUTES.divisions)
    );
    request.flush(catalogs);
    httpTestingController.verify();
  });

  it("getOffices retorna un catalogo de oficinas", () => {
    catalogsService.getOffices().subscribe((resp) => {
      expect(resp).toEqual(catalogs);
    });
    const request = httpTestingController.expectOne((req: HttpRequest<any>) =>
      req.url.includes(API_ROUTES.offices)
    );
    request.flush(catalogs);
    httpTestingController.verify();
  });
});
